/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeinheritancerework;

/**
 *
 * @author ripgg
 */
public class Circle extends Shape{
    private double r;
    private double pi = 22.0/7;
    
    public Circle (double r){
         if(r<=0){
            System.out.println("Error: Radius must more than zero!!!!!!!");
            return;
        }
        this.r=r;
    }
    
    
    public double calArea(){
        return this.r*this.r*this.pi;
    }
    
    public void Show(){
        System.out.println("Circle radius: "+this.r+" Area is: "+this.calArea());
    }

}
