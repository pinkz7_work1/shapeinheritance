/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeinheritancerework;

/**
 *
 * @author ripgg
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle (double base,double height){
        if(base==0 || height==0){
            System.out.println("Error: Base of Heigh must more than zero!!!!!!!");
            return;
        }
        this.base=base;
        this.height=height;
    }
    
    public double calArea(){
        return ((this.base*this.height)/2);
    }
    
    public void Show(){
        System.out.println("Triangle Base: "+this.base+" Triangle Height: "+this.height+" Area is: "+this.calArea());
    }
}
