/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeinheritancerework;

/**
 *
 * @author ripgg
 */
public class Rectangle extends Shape{
    private double width;
    private double height;
    
    public Rectangle (double width, double height){
        if(width<=0){
            System.out.println("Error: Width must more than zero!!!!!!!");
            return;
        } if(height<=0){
            System.out.println("Error: Height must more than zero!!!!!!!");
            return;
        }
        this.width=width;
        this.height=height;
    }
    
    
    public double calArea(){
        return this.width*this.height;
    }
    
    public void Show(){
        System.out.println("Rectangle Width: "+this.width+" Rectangle Height: "+this.height+" Area is: "+this.calArea());
    }
}
