/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.shapeinheritancerework;

/**
 *
 * @author ripgg
 */
public class Square extends Shape {
    private double side;
    
    public Square (double side){
        if(side<=0){
            System.out.println("Error: Width must more than zero!!!!!!!");
            return;
        }
        this.side=side;
    }
    
    
    public double calArea(){
        return this.side*this.side;
    }
    
    public void Show(){
        System.out.println("Square Side: "+this.side+" Area is: "+this.calArea());
    }
}
